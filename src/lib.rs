#![feature(i128_type)]
#![feature(plugin)]
#![feature(test)]
#![plugin(interpolate_idents)]

#[macro_use]
extern crate lazy_static;

pub mod fibonacci;
pub mod factorial;
