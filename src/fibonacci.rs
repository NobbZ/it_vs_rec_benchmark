use std::collections::HashMap;
use std::sync::{Arc, RwLock};

pub fn fib_iter(n: u128) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        _ => {
            let mut pp = 0;
            let mut p = 1;
            let mut r = 0;

            for _ in 2..n + 1 {
                r = p + pp;
                pp = p;
                p = r;
            }

            r
        }
    }
}

pub fn fib_rec_naive(n: u128) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        _ => fib_rec_naive(n - 1) + fib_rec_naive(n - 2),
    }
}

lazy_static!{
    static ref FIBMAP: Arc<RwLock<HashMap<u128, u128>>> = Arc::new(RwLock::new(HashMap::new()));
}

pub fn fib_rec_memo(n: u128) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        _ => {
            let map_lock = FIBMAP.read().unwrap();

            if !map_lock.contains_key(&n) {
                drop(map_lock);

                let result = fib_rec_memo(n - 1) + fib_rec_memo(n - 2);

                let mut map_lock_w = FIBMAP.write().unwrap();
                (*map_lock_w).insert(n, result);
            }

            let map_lock = FIBMAP.read().unwrap();
            map_lock[&n]
        }
    }
}

pub fn fib_rec_opt(n: u128) -> u128 {
    match n {
        0 => 0,
        1 => 1,
        _ => do_fib_rec_opt(n, 0, 1),
    }
}

fn do_fib_rec_opt(n: u128, pp: u128, p: u128) -> u128 {
    if n == 0 {
        return pp;
    }

    do_fib_rec_opt(n - 1, p, pp + p)
}

macro_rules! test_fib {
    ($name:ident) => { interpolate_idents! {
        #[test]
        fn [test_ $name _5]() {
            assert_eq!(5, $name(5));
        }

        #[test]
        fn [test_ $name _10]() {
            assert_eq!(55, $name(10));
        }

        #[test]
        fn [test_ $name _20]() {
            assert_eq!(6765, $name(20));
        }

        #[test]
        fn [test_ $name _30]() {
            assert_eq!(832040, $name(30));
        }
    }};
}

test_fib!(fib_iter);
test_fib!(fib_rec_naive);
test_fib!(fib_rec_memo);
test_fib!(fib_rec_opt);
