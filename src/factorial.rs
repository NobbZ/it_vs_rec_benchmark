pub fn fac_iter(n: u128) -> u128 {
    if n == 0 {
        return n;
    }

    let mut r = 1;

    for i in 1..n + 1 {
        r *= i;
    }

    r
}

pub fn fac_rec_naive(n: u128) -> u128 {
    if n <= 1 {
        return n;
    }

    n * fac_rec_naive(n - 1)
}

pub fn fac_rec_opt(n: u128) -> u128 {
    if n <= 1 {
        return n;
    }

    do_fac_rec_opt(n, 1)
}

fn do_fac_rec_opt(n: u128, r: u128) -> u128 {
    if n == 1 {
        return r;
    }

    do_fac_rec_opt(n - 1, r * n)
}

macro_rules! test_fac {
    ($name:ident) => { interpolate_idents! {
        #[test]
        fn [test_ $name _5]() {
            assert_eq!(120, $name(5));
        }

        #[test]
        fn [test_ $name _10]() {
            assert_eq!(3628800, $name(10));
        }

        #[test]
        fn [test_ $name _20]() {
            assert_eq!(2432902008176640000, $name(20));
        }

        #[test]
        fn [test_ $name _30]() {
            assert_eq!(265252859812191058636308480000000, $name(30));
        }
    }};
}

test_fac!(fac_iter);
test_fac!(fac_rec_naive);
test_fac!(fac_rec_opt);
