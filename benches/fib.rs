#![feature(i128_type)]

extern crate bench_stuff;

#[macro_use]
extern crate criterion;

use criterion::{Bencher, Criterion};
use std::time::Duration;

use bench_stuff::fibonacci::{fib_iter, fib_rec_memo, fib_rec_naive, fib_rec_opt};

fn fib_bench(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "fib_iter",
        |b: &mut Bencher, n: &u128| b.iter(|| fib_iter(*n)),
        vec![5, 10, 20, 30, 100],
    );
    c.bench_function_over_inputs(
        "fib_rec_naive",
        |b: &mut Bencher, n: &u128| b.iter(|| fib_rec_naive(*n)),
        vec![5, 10, 20, 30],
    );
    c.bench_function_over_inputs(
        "fib_rec_memo",
        |b: &mut Bencher, n: &u128| b.iter(|| fib_rec_memo(*n)),
        vec![5, 10, 20, 30, 100],
    );
    c.bench_function_over_inputs(
        "fib_rec_opt",
        |b: &mut Bencher, n: &u128| b.iter(|| fib_rec_opt(*n)),
        vec![5, 10, 20, 30, 100],
    );
}

criterion_group!(
    name = benches;
    config = Criterion::default()
        .sample_size(100)
        .warm_up_time(Duration::from_secs(10));
    targets = fib_bench
);
criterion_main!(benches);
