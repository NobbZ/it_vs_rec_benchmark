#![feature(i128_type)]

extern crate bench_stuff;

#[macro_use]
extern crate criterion;

use criterion::{Bencher, Criterion};
use std::time::Duration;

use bench_stuff::factorial::{fac_iter, fac_rec_naive, fac_rec_opt};

fn fac_bench(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "fac_iter",
        |b: &mut Bencher, n: &u128| b.iter(|| fac_iter(*n)),
        vec![5, 10, 20, 30],
    );
    c.bench_function_over_inputs(
        "fac_rec_naive",
        |b: &mut Bencher, n: &u128| b.iter(|| fac_rec_naive(*n)),
        vec![5, 10, 20, 30],
    );
    c.bench_function_over_inputs(
        "fac_rec_opt",
        |b: &mut Bencher, n: &u128| b.iter(|| fac_rec_opt(*n)),
        vec![5, 10, 20, 30],
    );
}

criterion_group!(
    name = benches;
    config = Criterion::default()
        .sample_size(100)
        .warm_up_time(Duration::from_secs(10));
    targets = fac_bench
);
criterion_main!(benches);
